import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {RouterModule,Routes} from '@angular/router';
import{AngularFireModule} from 'angularfire2';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { UsersService } from './users/users.service';
import { UserComponent } from './user/user.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { PostsComponent } from './posts/posts.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserFormComponent } from './user-form/user-form.component';

export const firebaseConfig = {
    apiKey: "AIzaSyAu6LY8SksKCAk4Uy5z-VUvvX7l0HkjB48",
    authDomain: "angular-d3b02.firebaseapp.com",
    databaseURL: "https://angular-d3b02.firebaseio.com",
    storageBucket: "angular-d3b02.appspot.com",
    messagingSenderId: "775209788373"
}

const appRoutes:Routes = [
  {path:'users', component:UsersComponent},
  {path:'posts', component:PostsComponent},
  {path:'', component:UsersComponent},
  {path:'**', component:PageNotFoundComponent},
]

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    UserComponent,
    SpinnerComponent,
    PostsComponent,
    PageNotFoundComponent,
    UserFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig)

  ],
  providers: [UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
